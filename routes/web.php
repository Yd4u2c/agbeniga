<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile/inbox', 'HomeController@index');

Route::get('profile/compose', 'HomeController@compose');

Route::post('sendmsg', 'MessageController@compose_message');

Route::post('prove', 'MessageController@prove')->name('prove');

Route::get('/profile/readmore/{id}','MessageController@readMore');

Route::get('/download/{file}', 'MessageController@download');

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::post('/send/email', 'MessageController@mail');




