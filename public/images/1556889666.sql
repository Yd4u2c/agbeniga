-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 24, 2019 at 08:41 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `techtank`
--

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `coursename` varchar(250) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `coursename`, `amount`, `date`) VALUES
(1, 'Digital Project Management', '200', '2019-04-22 16:02:01'),
(2, 'Digital Product Management', '300', '2019-04-22 16:02:01'),
(3, 'Digital Business Analysis', '400', '2019-04-22 16:02:01'),
(4, 'Digital Performance Management', '500', '2019-04-22 16:02:01'),
(5, 'Digital Specialist', '600', '2019-04-22 16:02:01'),
(6, 'User Research', '700', '2019-04-22 16:02:01'),
(7, 'Data Analysis', '800', '2019-04-22 16:02:01'),
(8, 'Content & Design', '900', '2019-04-22 16:02:01'),
(9, 'Digital Leadership', '100', '2019-04-22 16:02:01'),
(10, 'Operation Management', '90', '2019-04-22 16:02:01');

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_data_table`
--

CREATE TABLE `dashboard_data_table` (
  `id` int(11) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `surname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `course_status` varchar(200) NOT NULL,
  `course` varchar(200) NOT NULL,
  `second_choice` varchar(200) NOT NULL,
  `amount` varchar(200) NOT NULL,
  `payment_status` varchar(200) NOT NULL,
  `paid_amount` varchar(200) NOT NULL,
  `due_date` varchar(30) NOT NULL,
  `ref` varchar(200) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dashboard_data_table`
--

INSERT INTO `dashboard_data_table` (`id`, `firstname`, `surname`, `email`, `phone`, `course_status`, `course`, `second_choice`, `amount`, `payment_status`, `paid_amount`, `due_date`, `ref`, `date`) VALUES
(1, 'yakubu', 'abiola', 'yakubuabiola2003@gmail.com', '08030960928', 'Unpaid', 'Digital Product Management', 'Data Analysis', '33000', '', '', '', '', '2019-04-24 03:52:24');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `surname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `dob` varchar(70) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `course` varchar(255) NOT NULL,
  `second_choice` varchar(255) NOT NULL,
  `price` varchar(50) NOT NULL,
  `university` varchar(255) NOT NULL,
  `course_studied` varchar(255) NOT NULL,
  `experience` varchar(255) NOT NULL,
  `ethnicity` varchar(100) NOT NULL,
  `next_of_kin` varchar(255) NOT NULL,
  `next_of_kin_phone` varchar(255) NOT NULL,
  `interest` varchar(255) NOT NULL,
  `activate` int(11) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `date_registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `surname`, `email`, `password`, `phone`, `address`, `dob`, `gender`, `course`, `second_choice`, `price`, `university`, `course_studied`, `experience`, `ethnicity`, `next_of_kin`, `next_of_kin_phone`, `interest`, `activate`, `hash`, `status`, `type`, `date_registered`) VALUES
(4, 'yakubu', 'abiola', 'abiola@techcellent360global.com', 'df16f7ee30c30ef8eceb5ed517474c21', '08030960928', '6 goshen street afobaje estate ota', '2019-04-01', 'male', 'Digital Project Management', 'Digital Product Management', '7777', 'B.S.E', 'Forest Resources Management (Forestry)', 'Education', 'Igbo', 'yakubu damilola', '08134193440', 'Education', 0, 'abc06c32fbd7ecff59bd2cf57c3d93f0', 0, 'user', '2019-04-30 13:28:00'),
(7, 'yakubu', 'abiola', 'yakubuabiola2003@gmail.com', 'df16f7ee30c30ef8eceb5ed517474c21', '08030960928', '6 goshen street afobaje estate ota', '2019-04-03', 'male', 'Digital Product Management', 'Data Analysis', '33000', 'HND', 'Arabic and Islamic Studies', 'Health services', 'Igbo', 'yakubu damilola', '08030960928', 'Oil and gas', 1, 'ad4b7a14177e0a11ad5d099fa84a4866', 0, 'user', '2019-04-24 03:53:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coursename` (`coursename`);

--
-- Indexes for table `dashboard_data_table`
--
ALTER TABLE `dashboard_data_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `dashboard_data_table`
--
ALTER TABLE `dashboard_data_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
