@extends('profile.header')
@section('content')
<div class="col-lg-12 container">
@foreach($data as $user)
	<p>Sender's Name: {{$user->name}}</p>
	<p>Sender's Mail: {{$user->sender_email}}</p>
	<p>Subject: {{$user->subject}}</p>
	<p>Message: {{$user->message}}</p>
	<a href="/download/{{$user->file}}" class="text-right fa fa-file">Download attachment</a>
	@endforeach
</div>

@endsection