<?php

namespace App\Http\Controllers;

use App\Message;
use App\User;
use Session;
use Auth;
use ZipArchive;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMailable;

use Illuminate\Http\Request;

class MessageController extends Controller
{
    //
	public function compose_message(Request $request)
	{
    	#::::::::::VALIDATION:::::::::::
		$this->validate($request, [
			'Name' => 'required',
			'Email' => 'required',
			'receiver' => 'required',
			'area1' => 'required'
		]);

		//dd($request->all());
		$data = new Message();
		$data->Name = $request->Name;
		$data->sender_email = $request->Email;
		$data->receiver_email = $request->receiver;
		$data->message = $request->area1;
		$data->subject = $request->Subject;
		$data->code = $request->code;
		

		if ($request->hasFile('input_img')) 
		{
			$image = $request->file('input_img');
			$name = time().'.'.$image->getClientOriginalExtension();
			$destinationPath = public_path('/images');
			$image->move($destinationPath, $name);
			$data->file = $name;
			$data->save();
/**
			$message = 'Good day, A mail has just been sent to you from mail direct secured sender, use this code to decrypt you message: '.$request->code;
		Mail::to($request->receiver)->send(new SendMailable($message));
**/


// verification email sent to users email
		$urlin = "http://localhost:8000/login";
        $email = $request->receiver;
       # dd($email);
        $subject = "code ";
        $tag = "pp";
        $replyto = "yd4u2c@yahoo.com";
        //$email_code = $data['hash'];
        $subject1="".$subject;
        $message="
        <html>
        <head>
        <title>Decryption Code</title>
        </head>
        <body>
        <h2>Mail Direct</h2>
        <p>Your Account:</p>
        <p>Good day, A mail has just been sent to you from mail direct secured sender, use this code to decrypt you message: </p>
        <p>Below is the code for decryption</p>
        <h4> " . $request->code. " </h4>
        <p> please use the above code to proceed to decrypt </p>        
        <p>you can as well click the link below to login.</p>
        <h4> " .$urlin. " </h4>
        <p>Thank You!</P>
        </body>
        </html>
        ";
        $you ="hello";
      // $message = str_replace ("\r\n", "<br>", $message );
// working mailgun
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, 'api:key-e415d6a58ae45d0b8a389d17600555ee');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_URL, 
          'https://api.mailgun.net/v2/www.esoftchurch.com/messages');
        curl_setopt($ch, CURLOPT_POSTFIELDS, 
          array('from' => '2 tier autentication <admin@softchurch.com>',
            'to' => $you.' <'.$email.'>',
            'subject' => $subject1,
            'html' => $message,
            'text' => $message,
            'o:tracking'=>'yes',
            'o:tracking-clicks'=>'yes',
            'o:tracking-opens'=>'yes',
            'o:tag'=>$tag,
            'h:Reply-To'=>$replyto));
        $result = curl_exec($ch);
        curl_close($ch);
          // end of verification email from user


			Session::flash('success','Message sent successfully');
		//return redirect()->back()->withSuccess('IT WORKS!');
			return view('profile.compose');

		}
		
	}

	public function readmore($id)
	{
		$user = Message::where('id',$id)->first();

		$encrypt = Crypt::encryptString("encoded");

		$user->encrypt = $encrypt;
		//dd($user);
		return view('profile.read_more',compact('user'));
		
	}


	public function download($file_name) 
	{
		/**
		$path = public_path('images/');
		$files[] = public_path() . $path . uniqid() . '1543848392.pdf';
		$zipper->make($zip_filename)->add($files);
		**/

		$file_path = public_path('images/'.$file_name);
		return response()->download($file_path);
	}

	public function mail()
	{
		$name = 'Krunal';
		Mail::to('yd4u2c@yahoo.com')->send(new SendMailable($name));

		return 'Email was sent';
	}

	public function prove(Request $request)
	{
		$data = Message::where('id',$request['id'])
		->where('code', $request['key'])->get();
		if ($data->isempty()) {
			Session::flash('error', 'incorrect key');
			return redirect('profile/inbox');
		}
		else{
			return view('profile.decrypt', compact('data'));
		}
	}

}





