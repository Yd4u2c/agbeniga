<?php
namespace App\Http\Controllers;

use App\Message;

use Auth;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexs()
    {
        //dd('hello');
        return view('profile.inbox');
    }

    public function index(){
        $email = Auth::user()->email;
        $message = Message::where('receiver_email',$email)->get();
        //dd($message);

        return view('profile.inbox',compact('message'));
    }

    public function compose()
    {
        return view('profile.compose');
    }


}
