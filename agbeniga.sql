-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2019 at 05:53 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agbeniga`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sender_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `sender_email`, `receiver_email`, `message`, `file`, `created_at`, `updated_at`, `subject`, `code`) VALUES
(1, 'yakubu abiola', 'yakubuabiola2003@gmail.com', 'yd4u2c@yahoo.com', 'hhh', '1556889666.sql', '2019-05-03 12:21:06', '2019-05-03 12:21:06', 'try', '102986'),
(2, 'yakubu abiola', 'yakubuabiola2003@gmail.com', 'yd4u2c@yahoo.com', 'hhh', '1556890020.sql', '2019-05-03 12:27:00', '2019-05-03 12:27:00', 'try', '102986'),
(3, 'yakubu abiola', 'yakubuabiola2003@gmail.com', 'yd4u2c@yahoo.com', 'ikghfx', '1556890167.sql', '2019-05-03 12:29:27', '2019-05-03 12:29:27', 'try', '172608'),
(4, 'yakubu abiola', 'yakubuabiola2003@gmail.com', 'yd4u2c@yahoo.com', 'ikghfx', '1556890182.sql', '2019-05-03 12:29:42', '2019-05-03 12:29:42', 'try', '172608'),
(5, 'sample user', 'yd4u2c@yahoo.com', 'omolara.ayobee@gmail.com', 'to test for the <br>', '1557880419.jpg', '2019-05-14 23:33:40', '2019-05-14 23:33:40', 'sending', '583685'),
(6, 'yetty', 'yetty4u@yahoo.com', 'omolara.ayobee@gmail.com', 'to check for d link', '1557944036.jpg', '2019-05-15 17:13:56', '2019-05-15 17:13:56', 'correct', '416303'),
(7, 'sample user', 'yd4u2c@yahoo.com', 'omolara.ayobee@gmail.com', 'see the result', '1557946229.jpg', '2019-05-15 17:50:29', '2019-05-15 17:50:29', 'example', '569783');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_11_26_141013_create_messages_table', 1),
(4, '2018_12_07_084347_add_column', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'yakubu abiola', 'yakubuabiola2003@gmail.com', NULL, '$2y$10$Ab9KlN4sDFEMbWr.7UEs7e6ZkQi/g6nLQqGSNDvql0wecTD5K9Go2', '26bqjH1WipIMINRiueWIGlR67TxkG84Mqp70XIrcctWNNEwbYtn7qlVYQj2R', '2019-05-03 12:18:46', '2019-05-03 12:18:46'),
(2, 'sample user', 'yd4u2c@yahoo.com', NULL, '$2y$10$ehXQmOhbI0X2aO2HPDrfluGs12ISv6966mc.lPio1urJafYJkWuYa', 'eKECFkUuFrXqyBmVJV67H2Avy7MyTVGdU5ZD9H2svfh9fgPuzyZYCbRix9LY', '2019-05-03 12:20:00', '2019-05-03 12:20:00'),
(3, 'omolara', 'omolara.ayobee@gmail.com', NULL, '$2y$10$XC/EIi04ICp2hxdHKeXLVe0ave.ZFuUuC/iYNlxIz47cHyKgZ2k1a', 'LyVFJjOB1GbKpzI5uI1t0PJEwt4DpLXn9PI07jwC0lrMb0AspVTs3Ie8iJQV', '2019-05-14 23:30:57', '2019-05-14 23:30:57'),
(4, 'yetty', 'yetty4u@yahoo.com', NULL, '$2y$10$OFLt1lkXa2TnkIN5uptI/.1CWug57MaPTRFz1EnRcbb1JNQyABwGO', 'z7EeWL7LLNTKo5wgYWFrwi67fi7QPfnwFhov1vFRa1y9pXGiAWWNLJIYV0XF', '2019-05-15 17:11:03', '2019-05-15 17:11:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
