@extends('profile.header')
@section('content')
<div class="col-sm-12 edit">
	@if(Session::has('error'))
	<div class="alert alert-danger">
		{{ Session::get('error') }}
	</div>
	@endif
	
	<table class="col-sm-8 table-bordered">
		<tr>
			<th colspan="2"><center> <h4 class="txt-white">INBOX</h4></center></th>
		</tr>
		<tr class="txt-white">
			<th class="col-sm-4">Sender</th>
			<th>Subject</th>
		</tr>
		@foreach($message as $user)
		<tr>
			<td>
				<a href="readmore/{{ $user->id }}">{{ $user->name }}
				</a>
			</td>
			<td> 
				<a href="readmore/{{ $user->id }}">{{ $user->subject}}</a>
			</td>
		</tr>
		@endforeach

		
	</table>
	<p class="col-sm-4"></p>
</div>

<style type="text/css">
	.edit{
		height: 600px;
		background-color: rgba(200, 100, 0, 0.7);
		padding: 30px;
	}
	.spa{
		border: 1px solid #fff;
		height: 30px;
	}
	.txt-white{
		color: #fff;
	}
</style>
@endsection