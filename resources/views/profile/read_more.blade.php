@extends('profile.header')
@section('content')
<div class="col-lg-12 container">
	<div class="col-lg-8">
		{{ Crypt::encryptString($user->message) }}
		<a href="/download/{{$user->file}}" class="text-right fa fa-file">Zip attachment</a>
	</div>
	<dir class="col-lg-5">
		<form method="post" action="{{ route('prove') }}">
			<p>enter key sent to you mail to decrypt message and unzip file</p>
			{{ csrf_field() }}
			<input type="hidden" name="id" value="{{ $user->id }}">
			<div class="form-group">
				<label>Key</label>
				<input type="text" name="key" class="form-control" placeholder="Enter key here">
			</div>
			<input type="submit" name="submit" value="Decrypt message" class="btn btn-danger">
		</form>
	</dir>

</div>

@endsection